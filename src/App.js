import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import About from './About';
import Services from './Services';
import Template from './template';

class App extends Component {
   render() {
    
      return (
         <Router>
            <div>
               <Template/>
            </div>
         </Router>
      );
   }
}
export default App;