import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import Login from './Login';
import About from './About';
import Services from './Services';
import Footer from './footer';
import './App.css';
import logo from './logo.svg';
class Header extends Component {
    render() {
        var myStyle = {
            float:'right',
            padding:'10px 10px 10px 0px',
            listStyleType: "none",
            margin:'-2em 0 0 20px'
            
       }
       var my={
        textAlign: 'center',
        backgroundColor: "pink",
        padding:"50px 0 0 0px",
        margin:'0px'
       }
      return (
      <div>
          
          <ul style={my}>
          {/* <img src={logo} className="App-logo" alt='logo'/> */}
                  <li style={myStyle}><Link to={'/Services'}>Services</Link></li>
                  <li style={myStyle}><Link to={'/About'}>About</Link></li>
                  <li style={myStyle}><Link to={'/Login'}>Login</Link></li>
                  <li style={myStyle}><Link to={'/'}>Home</Link></li>
               </ul>
               <Switch>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/Login' component={Login} />
                  <Route exact path='/About' component={About} />
                  <Route exact path='/Services' component={Services} />
               </Switch>
          </div>
          );
    }
  }
  export default Header;